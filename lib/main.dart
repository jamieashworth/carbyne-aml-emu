import 'package:flutter/material.dart';
import './main_common.dart';

void main() {
  // Ok -- run the larger common main() code
  WidgetsFlutterBinding.ensureInitialized();
  mainCommon(null, null);
}
