// Setup common work we want to do for any invocation of the app..
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:dio/adapter.dart';
import 'package:logging/logging.dart';

import './app.dart';
import './globals.dart' as globals;
import './util/device.dart';
import './util/prefs.dart';
import './util/storage.dart';

Future<void> mainCommon(
    int defaultDataSourceIndex, String defaultDataSourceServerURL) async {
  // See if we are in debug mode or production (i.e. not debug)

  // Set Logger and level if we are not in debug mode
  if (globals.isInDebugMode) {
    // TODO(sjm): Maybe grab the logging level from command line?
    Logger.root.level = Level.ALL;
    Logger.root.onRecord.listen((LogRecord rec) {
      debugPrint(
          '${rec.level.name}: ${rec.time}: ${rec.loggerName} - ${rec.message}');
    });
  }

  final Logger logger = Logger('main_common');

  // Grab all the async data initialization we need here ..then start the main app loop..
  Future.wait(<Future<void>>[Prefs.init(), Storage.init(), Device.init()])
      .then((_) async {
    logger.fine('Initialization Futures completed.');

    // Set basic preferences if not already done..
    globals.isDarkTheme =
        Prefs.getBool(globals.AML_EMU_PREF_DARK_THEME_KEY, value: true);

    Prefs.setBool(globals.AML_EMU_PREF_DARK_THEME_KEY, globals.isDarkTheme);

    globals.phoneNumber = Prefs.getString(globals.AML_EMU_PREF_PHONE_NBR);
    globals.phoneIMEI = Prefs.getString(globals.AML_EMU_PREF_IMEI_NBR);
    globals.phoneICCID = Prefs.getString(globals.AML_EMU_PREF_ICCID_NBR);

    int sourceIndex = Prefs.getInt(globals.AML_EMU_PREF_SYSTEM);
    //int hmacIndex = Prefs.getInt(globals.AML_EMU_PREF_KEY);
    int hmacIndex = sourceIndex;

    // Get or default proxy attribuites
    globals.httpProxy = Prefs.getString(globals.AML_EMU_PREF_PROXY_ADDR);
    globals.httpProxyAuth = Prefs.getString(globals.AML_EMU_PREF_PROXY_AUTH);

    // If the data source is out of bounds reset to Dev to be safe
    if (sourceIndex >= globals.DataSource.values.length) {
      sourceIndex = 0;
    }
    globals.dataSource = globals.DataSource.values[sourceIndex];

    // If the hmac source is out of bounds reset to Dev to be safe
    if (hmacIndex >= globals.HmacKey.values.length) {
      hmacIndex = 0;
    }
    globals.hmacKey = globals.HmacKey.values[hmacIndex];

    // debugPaintSizeEnabled = true;
    // debugPaintBaselinesEnabled = true;
    // debugPaintPointersEnabled = true;

    // Create our Dio HTTPClient and set configuration options
    globals.httpClient = Dio();
    globals.httpClient.options.connectTimeout = 5000;

    // Set Proxy if we have it ..

    // Proxy?
    (globals.httpClient.httpClientAdapter as DefaultHttpClientAdapter)
        .onHttpClientCreate = (HttpClient client) {
      // Set our proxy if we have one ..
      client.findProxy = (uri) {
        // proxy requsts to defined string
        String proxy = globals.httpProxy.length > 0
            ? 'PROXY ${globals.httpProxy}'
            : 'DIRECT';
        debugPrint('findProxy invoked for uri $uri - returnung <$proxy>');
        return proxy;
      };
      // Ignore bad certs for self-signed stuff..
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
    };

    // ANd we're off to the races..
    runApp(CarbyneAMLEmu());
  });
}
