// Area to store shared resources across components..
import 'package:carbyne_aml_emulator/util/device.dart';
import 'package:flutter/material.dart';
// import 'package:native_widgets/native_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:fluro/fluro.dart';
import 'package:dio/dio.dart';

// Our location service access point
Location locationService;

// Hold a reference to our navigation tree..
GlobalKey<NavigatorState> navigatorKey;

// Hold our routing information
FluroRouter router;

Dio httpClient;
String httpProxy = ''; // Set to empty if no proxy desired..
String httpProxyAuth = '';

// Define our possible places to push the AML info..
// enum DataSource { dev, qa, staging, prodeu, produs }
enum DataSource {
  Dev,
  QA,
  Staging,
  EU,
  USGovE,
  USGovW,
  USOhio,
}
DataSource dataSource = DataSource.Dev; // Default to dev

// Define which key will be used when pushing the data to the cloud.
enum HmacKey {
  Dev,
  QA,
  Staging,
  EU,
  USGovE,
  USGovW,
  USOhio,
}
HmacKey hmacKey = HmacKey.Dev; // default key will be Dev

// Hold our device information
Device deviceInfo;

// Preferences
double textScaleFactor = 1.0;
bool isDarkTheme = false;
int refreshTime = 8; // Default time to update the AML data in seconds..
String phoneNumber;
String phoneIMEI;
String phoneICCID;

// Preference Keys
const String AML_EMU_PREF_DARK_THEME_KEY = 'isDarkTheme';
const String AML_EMU_PREF_REFRESH_KEY = 'refreshTime';
const String AML_EMU_PREF_PHONE_NBR = 'phone';
const String AML_EMU_PREF_IMEI_NBR = 'IMEI';
const String AML_EMU_PREF_ICCID_NBR = 'ICCID';
const String AML_EMU_PREF_SYSTEM = 'system';
const String AML_EMU_PREF_KEY = 'key';
const String AML_EMU_PREF_PROXY_ADDR = 'proxyAddr';
const String AML_EMU_PREF_PROXY_AUTH = 'proxyAuth';

// Handy little function..
bool get isInDebugMode {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}

bool isFutureStillInProgress(AsyncSnapshot<dynamic> _snapshot) {
  // See if the future is finished
  if ((_snapshot.connectionState == ConnectionState.done &&
          _snapshot.hasData) ||
      (_snapshot.connectionState == ConnectionState.none)) {
    return false;
  }
  return true;
}

// Handy utility class
class Utility {
  static void showAlertPopup(
      BuildContext context, String title, String detail) {
    void showAlertDialog<T>({BuildContext context, Widget child}) {
      showDialog<T>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => child,
      );
    }

    return showAlertDialog<Widget>(
        context: context,
        child: CupertinoAlertDialog(
          title: Text(title),
          content: Text(detail),
          actions: <CupertinoDialogAction>[
            CupertinoDialogAction(
                child: const Text('OK'),
                isDestructiveAction: false,
                onPressed: () {
                  Navigator.pop(context);
                }),
          ],
        ));
  }

  // Used in the Auth Login among other places...
  static void showDialogBox<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // The value passed to Navigator.pop() or null.
    });
  }
}
