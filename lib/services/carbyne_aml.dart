//
// Interface to the back-end AML Microservice
//
// import 'dart:io';
import 'dart:ffi';
import 'dart:math';
import 'dart:convert';

// import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:location/location.dart';
import 'package:logging/logging.dart';
import 'package:crypto/crypto.dart';

import '../globals.dart' as globals;
import '../util/device.dart';

final Logger logger = Logger('carbyne_aml');

// Define the various end points
const Map<globals.DataSource, String> _AMLService = {
  // globals.DataSource.dev: 'aml-service-dev.carbyneapi-dev.com/AMLService',
  // globals.DataSource.qa: 'aml-service-qa.carbyneapi-dev.com/AMLService',
  // globals.DataSource.staging: 'aml-service-stage.carbyneapi-dev.com/AMLService',
  // globals.DataSource.prod: 'aml-service-google1.carbyneapi.com/AMLService',
  globals.DataSource.Dev:
      'aml-service-dev-eu-west-1.carbyneapi-dev.com/AMLService',
  globals.DataSource.QA:
      'aml-service-qa-eu-west-1.carbyneapi-dev.com/AMLService',
  globals.DataSource.Staging:
      'aml-service-stage-eu-west-1.carbyneapi-dev.com/AMLService',
  globals.DataSource.EU: 'aml-service-eu-west-1.carbyneapi.com/AMLService',
  globals.DataSource.USGovW:
      'aml-service-us-gov-west-1.carbyneapi.com/AMLService',
  globals.DataSource.USGovE:
      'aml-service-us-gov-east-1.carbyneapi.com/AMLService',
  globals.DataSource.USOhio: 'aml-service-us-east-2.carbyneapi.com/AMLService',
  // globals.DataSource.prodeu: 'aml-service.carbyneapi.com/AMLService',
  // globals.DataSource.produs: 'aml-service-google1.carbyneapi.com/AMLService',
};

// Define the key being used for sending the data.
// If no Key is specified for the instance then nothing is calculated and it's sent with a bla
const Map<globals.HmacKey, String> _HMACKey = {
  globals.HmacKey.Dev:
      '\u0027-T3q>r2yP*HH}Nj?Ca(e@~X#(Epesd\u0024N\u0027tb277]9F]yM@AW]/kKPf7%[!*R?8bz',
  globals.HmacKey.QA:
      '\u0027-T3q>r2yP*HH}Nj?Ca(e@~X#(Epesd\u0024N\u0027tb277]9F]yM@AW]/kKPf7%[!*R?8bz',
  globals.HmacKey.Staging:
      '\u0027-T3q>r2yP*HH}Nj?Ca(e@~X#(Epesd\u0024N\u0027tb277]9F]yM@AW]/kKPf7%[!*R?8bz',
  globals.HmacKey.EU:
      '7[H6Fw6xj4<%s-KVD[):S6Bs{saAZm#5s)QE&PQ6w3/^j8@,s(%pR~6TW>e[}jj2',
  globals.HmacKey.USGovW:
      'jU82&nd-2H8o0G1Gmmjd^uci835kf499jQankaVU9sC[K0cmas(Dmc!jH-Ewc',
  globals.HmacKey.USGovE:
      'jU82&nd-2H8o0G1Gmmjd^uci835kf499jQankaVU9sC[K0cmas(Dmc!jH-Ewc',
  globals.HmacKey.USOhio:
      '7[H6Fw6xj4<%s-KVD[):S6Bs{saAZm#5s)QE&PQ6w3/^j8@,s(%pR~6TW>e[}jj2',
};

const String _AML_END_POINT = 'AML';
const String _ELS_END_POINT = 'ELS';

Random rng = Random();

// Return either the matching URL for the source or "dev" for default..
String findServiceURL(globals.DataSource source) {
  return _AMLService[source] ?? _AMLService[globals.DataSource.Dev];
}

// Fetch the key based on the destination.
String findKeyDest(globals.HmacKey key) {
  return _HMACKey[key] ?? _HMACKey[globals.HmacKey.Dev];
}

// Send this to one of our servivces..
// Currently we are going to use ELS service.. due to no authorization ..
Future<bool> sendLocationUpstream(LocationData location) async {
  logger.finer('[sendLoationUpstream] - Push AML Service invoked');

  // Ok - let's send it away ..
  final String serviceURL = findServiceURL(globals.dataSource);

  if (serviceURL == null) {
    logger.info(
        'Error! No URL post string found for data source <${globals.dataSource}');
    return false;
  }

  final String finalURL = 'https://$serviceURL/$_ELS_END_POINT';

  //logger.finer('Service End point is: $finalURL');

  // select the key based on the destiation.
  final String destKey = findKeyDest(globals.hmacKey);

  //Outputs to the log some of the variables for debugging.
  //logger.info(destKey);
  //logger.info('globals.dataSource is ${globals.dataSource}');
  //logger.info('globals.hmacKey ${globals.hmacKey}');

  if (destKey == null) {
    logger.info('No Key found for <${globals.dataSource}');
    return false;
  }

  final String mcc = '310'; // US for now
  final String mnc = '410'; // ATT for now ..

  // The IMEI may have spaces -- just remove them if it does..
  final imei = globals.phoneIMEI?.replaceAll(RegExp(r' '), '');

  // Add a "+" to the front of the number if not present..
  final String phoneNumber = globals.phoneNumber.startsWith('+')
      ? globals.phoneNumber
      : '+${globals.phoneNumber}';

  // Now build payload ..
  final Map<String, String> payload = {
    'v': '2',
    'emergency_number': '911',
    'source': 'CALL',
    'thunderbird_version': '209',
    'time': DateTime.now().millisecondsSinceEpoch.toString(),
    // 'gt_location_latitude' : '2',
    // 'gt_location_longitude' : '2',
    'location_latitude': location.latitude.toStringAsFixed(6),
    'location_longitude': location.longitude.toStringAsFixed(6),
    'location_altitude': location.altitude.toStringAsFixed(2),
    'location_time': DateTime.now().millisecondsSinceEpoch.toString(),
    'location_floor': '1', // Fixed for now..
    'location_source': 'gps',
    'location_accuracy': location.accuracy.toStringAsFixed(1),
    'location_vertical_accuracy': '5.0', // Obviously fake
    'location_confidence':
        _fakeConfidence(location.accuracy).toStringAsFixed(4), // Fake
    'location_bearing': location.heading.toStringAsFixed(4),
    'location_speed': location.speed.toStringAsFixed(4),
    'device_number': phoneNumber,
    'device_model': Device.machineName,
    'device_imsi': makeupIMSI(mcc, mnc, globals.phoneICCID),
    'device_imei': imei,
    'device_iccid': globals.phoneICCID,
    'cell_home_mcc': mcc, // US - for now
    'cell_home_mnc': mnc, // ATT -- for now
    'cell_network_mcc': mcc,
    'cell_network_mnc': mnc,
  };

  // Convert Map to String - Not needed at this time.
  //String payloadString = json.encode(payload);

  //HMAC signature test to see what it is processing as an output
  Object main() {
    Uri outgoingUri = new Uri(queryParameters: payload);

    //set the outogingUri to a string
    String url = outgoingUri.toString();

    //Remove the First Char from the URL string
    String rl = url.substring(1);

    //HMAC Signature...
    var key = utf8.encode(destKey);
    var bytes = utf8.encode(rl);
    var hmacSha1a = Hmac(sha1, key); //HMAC-SHA1
    var sha1Digest1 = hmacSha1a.convert(bytes);
    final hmacSha1 = sha1Digest1.toString();

    payload["hmac"] = hmacSha1;

    print('payload after appending $payload');
    return (payload);
  }

  main();

  //Prints the payload to verify it has the HMAC appended.
  //print(payload);

  // Ok -- now let's try to POST this bad boy to the server..

  // Test URL for a local machine to verify the URL that it's sending.
  // String testURL = 'http://10.0.0.158';
  //http.Response response = await http.post(testURL, body: payload);

  //Final send off.
  try {
    Response response = await globals.httpClient.post(
      finalURL,
      data: payload,
      options: Options(
        contentType: Headers.formUrlEncodedContentType,
      ),
    );
    logger.fine(
        'POST response is - status: <${response.statusCode} and <${response.data}>');
    return true;
  } on DioError catch (err) {
    logger.fine('POST response is - ERROR! message is : <${err.message}');
    return false;
  }
}

// Return some sort of confidence level ..
double _fakeConfidence(double accuracy) {
  if (accuracy < 10.0) {
    return 0.90 + (rng.nextDouble() / 20) - 0.05;
  }

  return 0.70 + (rng.nextDouble() / 10) - 0.05;
}

// Return a string that is something like an IMSI (which is nearly impossible to find on an iPhone)
String makeupIMSI(String mcc, mnc, String iccid) {
  return '$mcc$mnc' + iccid.substring(6, 15);
}

// Documentation on ESL post call..
//
// Sample:
//     v=2&thunderbird_version=209&emergency_number=101&source=CALL&time=1520359719970&location_latitude=32.0653545&
//        location_longitude=34.779573&location_time=1520359720092&location_altitude=54.5&location_vertical_accuracy=2.0&location_accuracy=16.363&location_confidence=0.6827
//        &location_source=wifi&device_number=%2B972546389832&device_model=Google+Pixel&device_imei=352531083948149&device_imsi=425071042854187&device_iccid=8997207104118541879
//        &cell_home_mcc=425&cell_home_mnc=07&cell_network_mcc=425&cell_network_mnc=07

//      //Data Type Key                         //Value Units   Example
//     Integer v                               //Version   -   2
//     String  emergency_number                //Emergency number dialed   -   911
//     ELSActivationSourceType source          //Source of activation (call, sms)  -   CALL
//     Integer thunderbird_version             //Version number for thunderbird module -   2800
//     Long    time                            //Timestamp of beginning of call    ms (unix time)  1.4381E+12
//     Double  gt_location_latitude            //Ground truth latitude (for testing)   degrees 37.4217829
//     Double  gt_location_longitude           //Ground truth longitude (for testing)  degrees -122.0884413
//     Double  location_latitude               //Latitude  degrees 37.4217845
//     Double  location_longitude              //Longitude degrees -122.0847413
//     Long    location_time                   //Timestamp of location ms (unix time)  1.4381E+12
//     Double  location_altitude               //Altitude (WGS84)  meters  4
//     Integer location_floor                  //Floor number  -   2
//     ELSLocationSourceType   location_source     //Location Source (gps, wifi, cell, unknown)    -   gps
//     Float   location_accuracy               //Accuracy  meters  20
//     Float   location_vertical_accuracy      //Vertical accuracy meters  2.5
//     Float   location_confidence             //Confidence in location accuracy   Percent (0-1)   0.6827
//     Float   location_bearing                //Bearing   degrees 156.7
//     Float   location_speed                  //Speed meters/second   1.2
//     String  device_number                   //Device phone number   -   1438101600
//     String  device_model                    //Device model  -   Motorola Nexus 6
//     String  device_imsi                     //IMSI  -   3.10261E+14
//     String  device_imei                     //IMEI  -   3.55458E+14
//     String  device_iccid                    //ICCID -   8.9148E+19
//     String  cell_home_mcc                   //Home MCC  -   310
//     String  cell_home_mnc                   //Home MNC  -   260
//     String  cell_network_mcc                //Network MCC   -   310
//     String  cell_network_mnc                //Network MNC   -   260
