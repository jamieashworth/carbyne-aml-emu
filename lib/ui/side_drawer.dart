//
// Main working widget area..
//
import 'package:flutter/material.dart';

Widget buildSideDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        AppBar(
          backgroundColor: Color(0xFF23245e),
          automaticallyImplyLeading: false,
          title: const Text('Choose'),
        ),
        _buildSettingsTile(context),
        _buildPossibleDivider(),
        _buildDeviceInfoTile(context),
        _buildSecureStorageInfoTile(context),
      ],
    ),
  );
}

Widget _buildPossibleDivider() {
  // only show this divider if we are a super user - if we are not displaying SA and SS data we do not need dividers.
  return Visibility(
    visible: true,
    child: const Divider(),
  );
}

Widget _buildSettingsTile(BuildContext context) {
  return ListTile(
      leading: const Icon(Icons.settings),
      title: const Text('Settings'),
      onTap: () {
        debugPrint('Settings Tapped..');
        Navigator.popAndPushNamed(context, '/settings');
      });
}

Widget _buildDeviceInfoTile(BuildContext context) {
  // only show this data if we are a super user
  return Visibility(
    visible: true,
    child: ListTile(
        leading: const Icon(Icons.devices),
        title: const Text('Device Info'),
        onTap: () {
          debugPrint('Device Info has been tapped');
          Navigator.popAndPushNamed(context, '/deviceInfo');
        }),
  );
}

Widget _buildSecureStorageInfoTile(BuildContext context) {
  // only show this data if we are a super user
  return Visibility(
    visible: true,
    child: ListTile(
        leading: const Icon(Icons.devices),
        title: const Text('Secure Storage Info'),
        onTap: () {
          debugPrint('Secure Storage Info has been tapped');
          Navigator.popAndPushNamed(context, '/storageInfo');
        }),
  );
}
