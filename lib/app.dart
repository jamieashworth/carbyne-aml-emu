import 'package:carbyne_aml_emulator/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:fluro/fluro.dart';
import 'package:location/location.dart';

import './globals.dart' as globals;

import './screens/device_info.dart';
import './screens/settings.dart';
import './screens/storage_info.dart';

import './util/carbyne_theme.dart';

class CarbyneAMLEmu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    globals.router = FluroRouter();
    return _CarbyneAMLEmuState();
  }
}

class _CarbyneAMLEmuState extends State<CarbyneAMLEmu> {
  String _initialRoute;

  @override
  void initState() {
    // Always put super.initState() at the start of the method
    super.initState();
    debugPrint('[CarbyneAMLEmu - initState()]');

    // Prefs.dumpPrefs();
    defineRoutes(globals.router);

    // Initialize our global NavigatorKey
    globals.navigatorKey = GlobalKey<NavigatorState>();

    // See if location is enabled and ask if it is not..
    globals.locationService = Location();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void defineRoutes(FluroRouter router) {
    //
    // NOTE -- ORDERING IS IMPORTANT!!!
    // THe define ordering is a bit important since the logic will take a route with parameters (if first) as matching a more specific
    // route later -- then barf ..
    //

    // The rest are pretty standard routing so no special handlers needed..
    router.define('/', handler: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return MyHomePage();
    }));

    // Handler our Scaffold Launch with or w/o the tab index
    // router.define('/home', handler: Handler(
    //     handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    //   logger.fine('[Router] - /home route invoked');
    //   return const MainScaffoldScreen(startScreen: 0);
    // }));

    router.define('/deviceInfo', handler: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return DeviceInfoScreen();
    }));

    router.define('/settings', handler: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return SettingsScreen();
    }));

    router.define('/storageInfo', handler: Handler(
        handlerFunc: (BuildContext context, Map<String, dynamic> params) {
      return StorageInfoScreen();
    }));
  }

  @override
  Widget build(BuildContext context) {
    debugPrint('[build()] - initial Route is $_initialRoute');

    // Figure out the right initial route .. we need critical data!
    if ((globals.phoneICCID?.isEmpty ?? true) ||
        (globals.phoneIMEI?.isEmpty ?? true) ||
        (globals.phoneNumber?.isEmpty ?? true)) {
      _initialRoute = 'settings';
    } else {
      _initialRoute = '/';
    }

    return DynamicTheme(
      defaultBrightness: Brightness.dark,
      data: (Brightness brightness) => CarbyneTheme.buildTheme(brightness),
      themedWidgetBuilder: (BuildContext context, ThemeData theme) {
        return MaterialApp(
          title: 'Carbyne AML EMUlator',
          theme: theme,
          initialRoute: _initialRoute,
          onGenerateRoute: globals.router.generator,
          navigatorKey: globals.navigatorKey,
        );
      },
    );
  }
}
