import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../util/storage.dart';

class StorageInfoScreen extends StatefulWidget {
  @override
  _StorageInfoState createState() => _StorageInfoState();
}

enum _Actions { deleteAll }
enum _ItemActions { delete, edit }

class _StorageInfoState extends State<StorageInfoScreen> {
  final FlutterSecureStorage _storage = FlutterSecureStorage();

  List<_SecItem> _items = <_SecItem>[];

  @override
  void initState() {
    super.initState();

    _readAll();
  }

  Future<void> _readAll() async {
    final Map<String, String> all = await _storage.readAll();
    setState(() {
      _items = all.keys
          .map((String key) => _SecItem(key, all[key]))
          .toList(growable: false);
    });
  }

  Future<void> _deleteAll() async {
    await _storage.deleteAll();
    _readAll();
  }

  Future<void> _addNewItem() async {
    final String key = _randomValue();
    final String value = _randomValue();

    await _storage.write(key: key, value: value);
    _readAll();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF23245e),
          title: const Text('Secure Storage'),
          actions: <Widget>[
            IconButton(onPressed: _addNewItem, icon: const Icon(Icons.add)),
            PopupMenuButton<_Actions>(
                onSelected: (_Actions action) {
                  switch (action) {
                    case _Actions.deleteAll:
                      _deleteAll();
                      break;
                  }
                },
                itemBuilder: (BuildContext context) =>
                    <PopupMenuEntry<_Actions>>[
                      const PopupMenuItem<_Actions>(
                        value: _Actions.deleteAll,
                        child: Text('Delete all'),
                      ),
                    ])
          ],
        ),
        body: ListView.builder(
          itemCount: _items.length,
          itemBuilder: (BuildContext context, int index) => ListTile(
            trailing: PopupMenuButton<_ItemActions>(
                onSelected: (_ItemActions action) =>
                    _performAction(action, _items[index]),
                itemBuilder: (BuildContext context) =>
                    <PopupMenuEntry<_ItemActions>>[
                      const PopupMenuItem<_ItemActions>(
                        value: _ItemActions.delete,
                        child: Text('Delete'),
                      ),
                      const PopupMenuItem<_ItemActions>(
                        value: _ItemActions.edit,
                        child: Text('Edit'),
                      ),
                    ]),
            title: Text(_items[index].value),
            subtitle: Text(_items[index].key),
          ),
        ),
      );

  Future<void> _performAction(_ItemActions action, _SecItem item) async {
    switch (action) {
      case _ItemActions.delete:
        await _storage.delete(key: item.key);
        _readAll();
        Storage.init();

        break;
      case _ItemActions.edit:
        final String result = await showDialog<String>(
            context: context,
            builder: (BuildContext context) => _EditItemWidget(item.value));
        if (result != null) {
          _storage.write(key: item.key, value: result);
          _readAll();
          Storage.init();
        }
        break;
    }
  }

  String _randomValue() {
    final Random rand = Random();
    final List<int> codeUnits = List<int>.generate(20, (int index) {
      return rand.nextInt(26) + 65;
    });

    return String.fromCharCodes(codeUnits);
  }
}

class _EditItemWidget extends StatelessWidget {
  _EditItemWidget(String text)
      : _controller = TextEditingController(text: text);

  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Edit item'),
      content: TextField(
        controller: _controller,
        autofocus: true,
      ),
      actions: <Widget>[
        TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Text('Cancel')),
        TextButton(
            onPressed: () => Navigator.of(context).pop(_controller.text),
            child: const Text('Save')),
      ],
    );
  }
}

class _SecItem {
  _SecItem(this.key, this.value);

  final String key;
  final String value;
}
