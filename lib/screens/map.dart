import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:logging/logging.dart';

import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

final Logger logger = Logger('map');

final DateFormat dateFormat = DateFormat.yMd().add_Hms();

class MapScreen extends StatefulWidget {
  MapScreen({Key key, @required this.location}) : super(key: key);

  final LocationData location;
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LocationData _location;

  @override
  void initState() {
    super.initState();

    logger.finer('[initState()]');
    _location = widget.location;
  }

  @override
  void dispose() {
    logger.finer('[dispose()]');

    super.dispose();
  }

  Widget _showMap(LocationData location) {
    Widget body;

    final LatLng latlng = LatLng(location.latitude, location.longitude);
    // final LatLng latlng = LatLng(51.5, -0.9);

    body = FlutterMap(
      options: MapOptions(center: latlng, zoom: 18.0),
      layers: [
        TileLayerOptions(
            urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            subdomains: ['a', 'b', 'c']),
        MarkerLayerOptions(
          markers: [
            Marker(
                //marker
                width: 25.0,
                height: 25.0,
                point: latlng,
                builder: (context) {
                  return Container(
                    child: IconButton(
                      icon: Icon(Icons.my_location),
                      iconSize: 25.0,
                      color: Color(0xff9045f7),
                      onPressed: () {
                        logger.fine('marker tapped');
                      },
                    ),
                  );
                }),
          ],
        ),
        CircleLayerOptions(
          circles: [
            CircleMarker(
              //radius marker
              point: latlng,
              color: Colors.blue.withOpacity(0.3),
              borderStrokeWidth: 3.0,
              borderColor: Colors.blue,
              useRadiusInMeter: true,
              radius: location.accuracy, //radius
            ),
          ],
        ),
      ],
    );

    return body;
  }

  @override
  Widget build(BuildContext context) {
    logger.finer('[build()]');

    String latString = widget.location.latitude.toStringAsFixed(3);
    String longString = widget.location.longitude.toStringAsFixed(3);

    return Container(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: Text(
                'Showing your current location at ($latString, $longString)'),
          ),
          Flexible(
            child: _showMap(_location),
          ),
        ],
      ),
    );
  }
}
