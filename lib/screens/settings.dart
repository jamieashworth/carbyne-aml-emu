///
/// Settings - Present a nice looking settings screen
///
import 'package:crypto/crypto.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
// import 'package:native_widgets/native_widgets.dart';
import 'package:flutter/cupertino.dart';

import 'package:package_info/package_info.dart';

import 'package:flutter/services.dart';
import 'package:logging/logging.dart';

import '../globals.dart' as globals;

import '../util/prefs.dart';

final Logger logger = Logger('home');

// Stateful widget for managing name data
class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

// State for managing fetching name data over HTTP
class _SettingsScreenState extends State<SettingsScreen> {
  // bool googleMapsInstalled = false;
  // GalleryTheme _galleryTheme = kAllGalleryThemes[0];
  String _versionName;
  String _versionBuild;
  final _formKey = GlobalKey<FormState>();

  int _systemRadioValue = 0;
  int _zoneRadioValue = 0;

  @override
  void initState() {
    logger.finer('[initState()');

    super.initState();
    initPlatformState();

    _versionName = 'Unknkown';
    _versionBuild = '1';

    // Initialize our system and zone radio buttons
    switch (globals.dataSource) {
      case globals.DataSource.Dev:
        _systemRadioValue = 0;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.QA:
        _systemRadioValue = 1;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.Staging:
        _systemRadioValue = 2;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.EU:
        _systemRadioValue = 3;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.USGovW:
        _systemRadioValue = 4;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.USGovE:
        _systemRadioValue = 5;
        _zoneRadioValue = 0;
        break;

      case globals.DataSource.USOhio:
        _systemRadioValue = 6;
        _zoneRadioValue = 0;
        break;
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      final PackageInfo info = await PackageInfo.fromPlatform();

      _versionName = info.version;
      _versionBuild = info.buildNumber;

      // _platform = Theme.of(context).platform.toString();
    } catch (err) {
      debugPrint(
          '[initPlatformState()] - Platform exception [$err] getting version info');
      _versionName = 'Unknown';
      _versionBuild = '???';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (mounted) {
      // Update the screen..
      setState(() {});
    }
  }

  void _handleTheme(bool value) {
    globals.isDarkTheme = value;
    Prefs.setBool(globals.AML_EMU_PREF_DARK_THEME_KEY, globals.isDarkTheme);
    setState(() {
      // globals.isDarkTheme = value;
      // globals.isDarkTheme = globals.isDarkTheme;
      // Prefs.setBool(globals.MM_PREF_DARK_THEME_KEY, globals.isDarkTheme);
      DynamicTheme.of(context)
          .setBrightness(value == true ? Brightness.dark : Brightness.light);
    });
  }

  void showDemoDialog<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    );
  }

  Widget _buildThemeTile() {
    return ListTile(
      leading: const Icon(Icons.color_lens),
      title: Text(
        'Dark Theme',
        textScaleFactor: globals.textScaleFactor,
      ),
      subtitle: Text(
        'Black and Grey Theme',
        textScaleFactor: globals.textScaleFactor,
      ),
      trailing: CupertinoSwitch(
        onChanged: _handleTheme,
        value: Theme.of(context).brightness == Brightness.dark,
      ),
    );
  }

  void _handleSystemRadioValueChange(int value) {
    logger.fine('System Radio Button Changed to <$value>');

    switch (value) {
      case 0:
        globals.dataSource = globals.DataSource.Dev;
        globals.hmacKey = globals.HmacKey.Dev;
        break;

      case 1:
        globals.dataSource = globals.DataSource.QA;
        globals.hmacKey = globals.HmacKey.QA;
        break;

      case 2:
        globals.dataSource = globals.DataSource.Staging;
        globals.hmacKey = globals.HmacKey.Staging;
        break;

      case 3: // Prod has 2 cases.. need further checking..
        globals.dataSource = globals.DataSource.EU;
        globals.hmacKey = globals.HmacKey.EU;
        break;

      case 4: // Prod has 2 cases.. need further checking..
        globals.dataSource = globals.DataSource.USGovW;
        globals.hmacKey = globals.HmacKey.USGovW;
        break;

      case 5: // This has been added so there is two us GOV destinations to use
        globals.dataSource = globals.DataSource.USGovE;
        globals.hmacKey = globals.HmacKey.USGovE;
        break;

      case 6: // This has been added so there is two us GOV destinations to use
        globals.dataSource = globals.DataSource.USOhio;
        globals.hmacKey = globals.HmacKey.USOhio;
        break;
    }

    // Save to our preferences
    Prefs.setInt(globals.AML_EMU_PREF_SYSTEM, globals.dataSource.index);

    // Now change the UI
    setState(() {
      _systemRadioValue = value;
    });
  }

  // Allow them to select what system to send the info to..
  Widget _buildSystemTile() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text(
          'Select System :',
          style: new TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          ),
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Radio(
              value: 0,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'Dev',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              value: 1,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'QA',
              style: new TextStyle(
                fontSize: 16.0,
              ),
            ),
            new Radio(
              value: 2,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'Staging',
              style: new TextStyle(fontSize: 16.0),
            ),
          ],
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Radio(
              value: 3,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'EU',
              style: new TextStyle(fontSize: 14.0),
            ),
            new Radio(
              value: 4,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'US-GovW',
              style: new TextStyle(fontSize: 14.0),
            ),
            //Added for the two different GovCloud instances
            new Radio(
              value: 5,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'US-GovE',
              style: new TextStyle(fontSize: 14.0),
            ),
            new Radio(
              value: 6,
              groupValue: _systemRadioValue,
              onChanged: _handleSystemRadioValueChange,
            ),
            new Text(
              'US-Ohio',
              style: new TextStyle(fontSize: 14.0),
            )
          ],
        ),
      ],
    );
  }

  void _handleZoneRadioValueChange(int value) {
    logger.fine('Zone Radio Button Changed to <$value>');

    // We must be in Prod system for us to even get here...
    globals.dataSource = globals.DataSource.EU;
    //globals.hmacKey = globals.DataSource.prod;
    // if (value == 0) {
    //   globals.dataSource = globals.DataSource.produs;
    // } else {
    //   globals.dataSource = globals.DataSource.prodeu;
    // }

    // Save to our preferences
    Prefs.setInt(globals.AML_EMU_PREF_SYSTEM, globals.dataSource.index);

    // Now show the UI
    setState(() {
      _zoneRadioValue = value;

      switch (_zoneRadioValue) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
        case 3:
          break;
        case 4:
          break;
        case 5:
          break;
        case 6:
          break;
      }
    });
  }

  // Allow them to select what system to send the info to..
  Widget _buildZoneTile() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text(
          'Select Production Zone :',
          style: new TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          ),
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Radio(
              value: 0,
              groupValue: _zoneRadioValue,
              onChanged: _handleZoneRadioValueChange,
            ),
            new Text(
              'USA',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              value: 1,
              groupValue: _zoneRadioValue,
              onChanged: _handleZoneRadioValueChange,
            ),
            new Text(
              'UK',
              style: new TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildPhoneTile() {
    return TextFormField(
      autocorrect: false,
      initialValue: globals.phoneNumber ?? '',
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
          labelText:
              "This Phone's Number (w/ Counry Code - '+' prefix not needed)"),
      maxLength: 15,
      validator: (value) {
        if (value.isEmpty) {
          return 'Your phone # is a required field.';
        }
        if (value.length < 9) {
          return 'Phone # looks a little short. Please remember to add the country code to the front';
        }
        return null;
      },
      onSaved: (val) {
        globals.phoneNumber = val;
        Prefs.setString(globals.AML_EMU_PREF_PHONE_NBR, globals.phoneNumber);
      },
    );
  }

  Widget _buildIMEITile() {
    return TextFormField(
      autocorrect: false,
      initialValue: globals.phoneIMEI ?? '',
      keyboardType: TextInputType.number,
      decoration: InputDecoration(labelText: "This Phone's IMEI Number"),
      maxLength: 19,
      validator: (value) {
        if (value.isEmpty) {
          return 'Your phone IMEI is a required field.';
        }
        if (value.length < 15) {
          return 'The IMEI # looks a little short. Please copy/paste from Settings->General->About';
        }
        return null;
      },
      onSaved: (val) {
        globals.phoneIMEI = val;
        Prefs.setString(globals.AML_EMU_PREF_IMEI_NBR, globals.phoneIMEI);
      },
    );
  }

  Widget _buildICCID() {
    return TextFormField(
      autocorrect: false,
      initialValue: globals.phoneICCID ?? '',
      keyboardType: TextInputType.number,
      decoration: InputDecoration(labelText: "This Phone's ICCID Number"),
      maxLength: 21,
      validator: (value) {
        if (value.isEmpty) {
          return 'Your phone ICCDI is a required field.';
        }
        if (value.length < 19) {
          return 'The ICCID # looks a little short. Please copy/paste from Settings->General->About';
        }
        return null;
      },
      onSaved: (val) {
        globals.phoneICCID = val;
        Prefs.setString(globals.AML_EMU_PREF_ICCID_NBR, globals.phoneICCID);
      },
    );
  }

  Widget _buildProxy() {
    return TextFormField(
      autocorrect: false,
      initialValue: globals.httpProxy ?? '',
      keyboardType: TextInputType.text,
      decoration: InputDecoration(labelText: 'Optional Proxy address:port'),
      onSaved: (val) {
        // Only force a change if something changed...
        if (globals.httpProxy != val) {
          logger.finer('Proxy changed ..resetting');
          globals.httpProxy = val;
          Prefs.setString(globals.AML_EMU_PREF_PROXY_ADDR, globals.httpProxy);
        }
      },
    );
  }

  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildPhoneTile(),
            _buildIMEITile(),
            _buildICCID(),
            _buildProxy(),
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: RaisedButton(
                onPressed: () {
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    Navigator.pop(context, '/');
                  }
                },
                child: Text('Save'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF23245e),
        title: Text(
          'Settings',
          textScaleFactor: globals.textScaleFactor,
        ),
      ),
      body: SingleChildScrollView(
          child: SafeArea(
        child: ListBody(
          children: <Widget>[
            const SizedBox(
              height: 10.0,
            ),
            _buildThemeTile(),
            const Divider(
              height: 20.0,
            ),
            _buildSystemTile(),
            // _systemRadioValue == 3 ? _buildZoneTile() : Container(),
            const Divider(
              height: 20.0,
            ),
            _buildForm(),
            const Divider(
              height: 20.0,
            ),
            Text(
              'Version: $_versionName ($_versionBuild)',
              textAlign: TextAlign.center,
            ),
          ],
        ),
      )),
    );
  }
}
