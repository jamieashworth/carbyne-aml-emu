import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:logging/logging.dart';

import '../globals.dart' as globals;

import '../services/carbyne_aml.dart';
import '../ui/side_drawer.dart';
import './map.dart';

final Logger logger = Logger('home');

final DateFormat dateFormat = DateFormat.yMd().add_Hms();

// Define the colors of the circle here..
const Color COLOR_READY = Colors.green;
const Color COLOR_UPDATING = Colors.lightBlueAccent;
const Color COLOR_WARNING = Colors.amber;
const Color COLOR_ERROR = Colors.redAccent;
const Color BACKGROUND_COLOR = Color(0xFF23245e);

enum PageState { HomeState, MapState }

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  AppLifecycleState _notification;
  PageState _pageState = PageState.HomeState;
  final Location _locationService = globals.locationService;
  LocationData _currentLocation;
  Timer _locationTimer;
  Timer _warningTimer;
  int _refreshSeconds = globals.refreshTime;
  PermissionStatus _permission;
  bool _listenerRegistered = false;
  DateTime _lastSentTime;
  DateTime _backgroundStart;
  globals.DataSource _priorDatasource = globals.dataSource;
  Color _circleColor;

  String _error = '...Initializing ...';

  @override
  void initState() {
    logger.fine('[initState()]');
    super.initState();

    // Delay a second then check for location ..
    _circleColor = COLOR_WARNING;
    _locationTimer = Timer(Duration(seconds: 1), _initPlatformState);

    // Add observer to see when app goes foreground -> background etc.
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    logger.fine('[dispose()]');

    // Remove our listener
    WidgetsBinding.instance.removeObserver(this);

    // ANd remove the timer ..
    _locationTimer?.cancel();
    _warningTimer?.cancel();

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    logger.fine('[didChangeApplicationState] : state <$state>');

    // Change our periodic timer based on current app state
    // If we are active then speed up the refresh.. else slow it down
    _locationTimer?.cancel();
    _warningTimer?.cancel();
    switch (state) {
      case AppLifecycleState.resumed:
        {
          _refreshSeconds = 8;
          _forceUpdate();
        }
        break;

      default:
        {
          _refreshSeconds = 15;
          _backgroundStart = DateTime.now();
          _circleColor = COLOR_WARNING;
        }
    }

    // Set up the new timer ...
    logger.finer('Setting update refresh to $_refreshSeconds seconds');
    _locationTimer =
        Timer.periodic(Duration(seconds: _refreshSeconds), _setLocationData);

    // Now do a nice refresh..
    setState(() {
      _notification = state;
    });
  }

  void _forceUpdate() async {
    await _setLocationData(_locationTimer);
  }

  void _updateCircle() {
    setState(() {
      _circleColor = COLOR_WARNING;
    });
  }

  // Toggle the actually body back and forth ...
  void _switchView() {
    setState(() {
      _pageState = _pageState == PageState.HomeState
          ? PageState.MapState
          : PageState.HomeState;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  void _initPlatformState() async {
    logger.finer('[_initPlatformFromState]');

    await _locationService.changeSettings(
        accuracy: LocationAccuracy.high, interval: globals.refreshTime * 1000);

    _error = '';

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      logger.fine('Service status: $serviceStatus');
      if (serviceStatus) {
        logger.fine('Checking if permission granted..');
        _permission = await _locationService.requestPermission();
        print("Permission: $_permission");
        if (_permission == PermissionStatus.granted) {
          _setLocationData(_locationTimer);
        } else {
          _error =
              'Location Not Enabled! Go to Settings to enable location for AML Emu';
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          return _initPlatformState();
        }
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _error = e.message;
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _error = e.message;
      }
      _currentLocation = null;
    }

    // If we have permission set a recurring timer to just update the location ..
    if (_permission == PermissionStatus.granted) {
      _locationTimer?.cancel();
      _warningTimer?.cancel();
      _locationTimer =
          Timer.periodic(Duration(seconds: _refreshSeconds), _setLocationData);

      // SJM -- Also do background location updates...
      // await _registerListener();
    }

    // Force a screen refresh
    setState(() {
      // Nothing..
    });

    return;
  }

  static void backgroundCallback(List<LocationData> locations) {
    logger.finer(
        '[backgroundCallback] - BACKGROUND!! - Location data received from background: $locations');
    print('Location data received from background: $locations');
  }

  Future<void> _registerListener() async {
    if (_listenerRegistered) {
      return;
    }

    logger.finer('[_registerListener] Registering background location getter');
    // bool statusBackgroundLocation =
    //     await _locationService.registerBackgroundLocation(backgroundCallback);

    // logger.fine(
    //     "[_registerListener] - statusBackgroundLocation: $statusBackgroundLocation");
    // _listenerRegistered = statusBackgroundLocation;
  }

  Future<void> _removeListener() async {
    if (_listenerRegistered) {
      logger
          .finer('[_removeListener] Unregistering background location getter');
      // await _locationService.removeBackgroundLocation();
      // _listenerRegistered = false;
    }
  }

  Future<void> _setLocationData(Timer timer) async {
    logger.finer('[_setLocationData()]');
    setState(() {
      _circleColor = COLOR_UPDATING;
    });

    _currentLocation = await globals.locationService.getLocation();

    logger.finer(
        'Got location data of Lat<${_currentLocation.latitude}> - Long<${_currentLocation.longitude}>');

    // If our data source happen to change -- reset the prior data so we are forced to send again
    if (_priorDatasource != globals.dataSource) {
      logger.finer('Data source changed -- forcing refresh');
      _priorDatasource = globals.dataSource;
    }

    final bool success = await sendLocationUpstream(_currentLocation);

    setState(() {
      if (success) {
        _circleColor = COLOR_READY;
        _error = 'Success!';
        _lastSentTime = DateTime.now();
        _warningTimer = Timer(
            Duration(seconds: (_refreshSeconds / 2).floor()), _updateCircle);
      } else {
        _error = 'Failed to send data to AML Service';
        _circleColor = COLOR_ERROR;
      }
    });
  }

  // Return either a map or the text screen with the nice Emu :)
  Widget _mainBody() {
    logger.finer('[_mainBody()]');

    Widget body;
    if (_pageState == PageState.HomeState) {
      body = _homeBody();
    } else {
      body = MapScreen(location: _currentLocation);
    }

    return body;
  }

  Widget _homeBody() {
    final double time = _currentLocation?.time ?? 0;
    final DateTime dateTime = time > 0
        ? DateTime.fromMillisecondsSinceEpoch(time.ceil() * 1000)
        : null;
    final String formatTime =
        dateTime == null ? 'Unknown Time' : dateFormat.format(dateTime);
    final String formatSentTime =
        _lastSentTime == null ? 'Unknown' : dateFormat.format(_lastSentTime);

    // Choose appropriate image of Sheila based on dark/light
    final String imageAsset = globals.isDarkTheme
        ? 'assets/Carbyne Logo.gif'
        : 'assets/Carbyne Logo.gif';
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Image.asset(imageAsset, height: 200.0, width: 200.0),
          padding: EdgeInsets.only(top: 16.0),
        ),
        Container(
          height: 50.0,
        ),
        Container(
          height: 65.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _circleColor,
          ),
        ),
        Container(
          height: 50.0,
        ),
        Text(
          'AML Location Information Being Sent:',
        ),
        Text(globals.dataSource.toString().split('.').last),
        Container(
          height: 15.0,
        ),
        Text('Lat: ${_currentLocation?.latitude}'),
        Text('Long: ${_currentLocation?.longitude}'),
        Text('Alt: ${_currentLocation?.altitude}'),
        Text('Accuracy: ${_currentLocation?.accuracy}'),
        Text('Speed: ${_currentLocation?.speed}'),
        Text('Speed Accuracy: ${_currentLocation?.speedAccuracy}'),
        Container(
          height: 15.0,
        ),
        Text(
          _error,
          style: Theme.of(context).textTheme.bodyText2,
        ),
        Text('Time of Location Fix: $formatTime'),
        Text('Last time sent to AML: $formatSentTime')
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    logger.finer('[build()]');
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF23245e),
        title: Text('Carbyne AML Emulation'),
      ),
      drawer: buildSideDrawer(context),
      body: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(16.0),
        child: _mainBody(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _switchView,
        backgroundColor: Color(0xFF23245e),
        tooltip: 'Switch View',
        child: Icon(Icons.map),
      ),
    );
  }
}
