///
/// Copyright (C) 2018 MissionMode Solutions
///
///
library device;

import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:logging/logging.dart';

// import 'package:flutter_udid/flutter_udid.dart';

/// Device Information our App is running on
class Device {
  static final Logger logger = Logger('Device');

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  /// In case the developer does not explicitly call the init() function.
  static Map<String, dynamic> deviceData = <String, dynamic>{};
  static bool hasBiometrics = true;
  static String biometricType;
  static bool isTablet = false;
  static String platformName;
  static String machineName;
  static String machineType;
  static String udid;

  // Set the common info based on device being an iOS thingy..
  /// Initialize the SecuredStorage object in the State object's iniState() function.
  static Future<void> init() async {
    // Use the package to get the udid
    // udid = await FlutterUdid.udid;

    // Ok -- grab the other information..
    if (Platform.isAndroid) {
      platformName = 'Android';
      final AndroidDeviceInfo androidData = await deviceInfoPlugin.androidInfo;
      deviceData = _readAndroidBuildData(androidData);
      machineName = deviceData['model'];
      machineType = deviceData['product'];
      udid = deviceData['id'];
      hasBiometrics = false;
      biometricType = 'None';
    } else if (Platform.isIOS) {
      platformName = 'iOS';
      deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      _setiOSDeviceInfo();
    }
  }

  /// Best to clean up by calling this function in the State object's dispose() function.
  static void dispose() {
    deviceData = null;
  }

  // // Debug dump of the Device Data
  // static void _dumpDeviceInfo() {
  //   // Iterate over the map and print out the contents
  //   logger.fine('DEVICE INFO DUMP');
  //   logger.fine('----------------');
  //   deviceData.forEach((k, v) => logger.fine('$k: $v'));
  // }

  // Set the common info based on device being an iOS thingy..
  static void _setiOSDeviceInfo() {
    // _dumpDeviceInfo();

    // The local name of the machine given by the User..
    machineName = deviceData['name'];
    logger.fine('[setiOS()] - machineName is $machineName');

    machineType = deviceData['utsname.machine:'];
    logger.fine('[setiOS()] - machineType is $machineType');

    // See if the machineType is an emulator and adjust ..
    if (machineType == 'i386' || machineType == 'x86_64') {
      // final Map<String, String> simEnv = Platform.environment;
      // logger.fine('DEVICE ENV DUMP');
      // logger.fine('----------------');
      // simEnv.forEach((k, v) => logger.fine('$k: $v'));
      // TODO(sjm): -- figure out a better way than hard coding the simName?
      final String simName =
          machineName.startsWith('iPhone X') ? 'iPhone10' : machineName;
      logger.fine('[setiOS()] - SIM platform is $simName');
      machineName = simName;
    }

    // Grab a device UUID
    udid = deviceData['identifierForVendor'];

    // See what device type we have ..
    final String model = deviceData['model'];
    isTablet = model.startsWith('iPad') ? true : false;
  }

  static Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    logger.fine('[DeviceInfoScreen] - _readAndroidBuildData');
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
    };
  }

  static Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  // Define our access routines..
  static bool get isIOS => Platform.isIOS;
  static bool get isAndroid => Platform.isAndroid;
  static String get deviceString =>
      '$platformName - $machineName<$machineType>';
}
