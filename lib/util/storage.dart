///
/// Copyright (C) 2019 Steven J. McDowall
///
///
library storage;

import 'dart:async';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// The App's Preferences.
class Storage {
  static final FlutterSecureStorage _storage = FlutterSecureStorage();
  static Map<String, String> _storageContents;

  /// In case the developer does not explicitly call the init() function.
  static bool _initCalled = false;

  /// Initialize the SecuredStorage object in the State object's iniState() function.
  static Future<void> init() async {
    _initCalled = true;
    _storageContents = await _storage.readAll();
  }

  /// Best to clean up by calling this function in the State object's dispose() function.
  static void dispose() {
    _initCalled = false;
    _storageContents = null;
  }

  /// Returns all keys in the secured storage.
  static Set<String> getKeys() {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getKeysF() instead. SecuredStorage not ready yet!');
    return _storageContents.keys.toSet();
  }

  /// Returns a Future.
  static Future<Set<String>> getKeysF() async {
    final Map<String, String> all = await _storage.readAll();
    final Set<String> _items = all.keys.toSet();
    return _items;
  }

  // static Future<void> _dumpStorage() async {
  //   // Iterate over the map and print out the contents
  //   logger.finer('STORAGE DUMP');
  //   logger.finer('------------');
  //   if (!_initCalled) {
  //     await init();
  //   }
  //   _storageContents.forEach((String k, String v) => logger.finer('$k: $v'));
  // }

  /// Reads a value of any type from persistent storage.
  static dynamic get(String key) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storage != null,
        'Maybe call Storage.getF(key) instead. SecuredStorage not ready yet!');
    return _storageContents[key];
  }

  /// Returns a Future.
  static Future<dynamic> getF(String key) async {
    dynamic value;
    if (_storageContents == null) {
      value = await _storage.read(key: key);
    } else {
      value = get(key);
    }
    return value;
  }

  static bool getBool(String key, [bool defaultValue = false]) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getBoolF(key) instead. SecuredStorage not ready yet!');
    final String keyValue = _storageContents[key]?.toLowerCase();
    return keyValue == null ? defaultValue : keyValue == 'true';
  }

  /// Returns a Future.
  static Future<bool> getBoolF(String key, {bool defaultValue = false}) async {
    bool value;
    if (_storage == null) {
      value = (await _storage.read(key: key)).toLowerCase() == 'true';
      _storageContents[key] = value.toString();
    } else {
      value = getBool(key, defaultValue);
    }
    return value;
  }

  static int getInt(String key) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getIntF(key) instead. SecuredStorage not ready yet!');
    final String value = _storageContents[key];
    return value == null ? 0 : int.parse(value);
  }

  /// Returns a Future.
  static Future<int> getIntF(String key) async {
    int value;
    if (_storage == null) {
      value = int.parse(await _storage.read(key: key));
    } else {
      value = getInt(key);
    }
    return value;
  }

  static double getDouble(String key) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getDoubleF(key) instead. SecuredStorage not ready yet!');
    final String value = _storageContents[key];
    return value == null ? 0.0 : double.parse(value);
  }

  /// Returns a Future.
  static Future<double> getDoubleF(String key) async {
    double value;
    if (_storageContents == null) {
      value = double.parse(await _storage.read(key: key));
    } else {
      value = getDouble(key);
    }
    return value;
  }

  static String getString(String key) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getStringF(key)instead. SecuredStorage not ready yet!');
    return _storageContents[key];
  }

  /// Returns a Future.
  static Future<String> getStringF(String key) async {
    String value;
    if (_storageContents == null) {
      value = await _storage.read(key: key);
    } else {
      value = getString(key);
    }
    return value;
  }

  static List<String> getStringList(String key) {
    assert(_initCalled,
        'Storage.init() must be called first in an initState() preferably!');
    assert(_storageContents != null,
        'Maybe call Storage.getStringListF(key) instead. SecuredStorage not ready yet!');
    final String jsonString = _storageContents[key];
    if (jsonString == null) {
      return null;
    }

    // Build our return type...
    final List<String> returnList = <String>[];
    jsonDecode(jsonString, reviver: (Object k, Object v) {
      if (v is String) {
        returnList.add(v);
      }
      return null;
    });

    return returnList;
  }

  /// Returns a Future.
  static Future<List<String>> getStringListF(String key) async {
    List<String> value;
    if (_storageContents == null) {
      final String jsonString = await _storage.read(key: key) ?? '[""]';
      value = jsonDecode(jsonString);
    } else {
      value = getStringList(key);
    }
    return value;
  }

  /// Saves a boolean [value] to persistent storage in the background.
  /// If [value] is null, this is equivalent to calling [remove()] on the [key].
  static Future<bool> setBool(String key, bool value) async {
    final String stringValue = value.toString();
    await _storage.write(key: key, value: stringValue);
    _storageContents[key] = stringValue;
    return true;
  }

  /// Saves an integer [value] to persistent storage in the background.
  /// If [value] is null, this is equivalent to calling [remove()] on the [key].
  static Future<bool> setInt(String key, int value) async {
    final String stringValue = value.toString();
    await _storage.write(key: key, value: stringValue);
    _storageContents[key] = stringValue;
    return true;
  }

  /// Saves a double [value] to persistent storage in the background.
  /// Android doesn't support storing doubles, so it will be stored as a float.
  /// If [value] is null, this is equivalent to calling [remove()] on the [key].
  static Future<bool> setDouble(String key, double value) async {
    final String stringValue = value.toString();
    await _storage.write(key: key, value: stringValue);
    _storageContents[key] = stringValue;
    return true;
  }

  /// Saves a string [value] to persistent storage in the background.
  /// If [value] is null, this is equivalent to calling [remove()] on the [key].
  static Future<bool> setString(String key, String value) async {
    await _storage.write(key: key, value: value);
    _storageContents[key] = value;
    return true;
  }

  /// Saves a list of strings [value] to persistent storage in the background.
  /// If [value] is null, this is equivalent to calling [remove()] on the [key].
  static Future<bool> setStringList(String key, List<String> value) async {
    final String stringValue = jsonEncode(value);
    await _storage.write(key: key, value: stringValue);
    _storageContents[key] = stringValue;
    return true;
  }

  /// Removes an entry from persistent storage.
  static Future<bool> remove(String key) async {
    await _storage.delete(key: key);
    _storageContents.remove(key);
    return true;
  }

  /// Completes with true once the user preferences for the app has been cleared.
  static Future<bool> clear() async {
    await _storage.deleteAll();
    _storageContents.clear();
    return true;
  }
}
