import 'package:flutter/material.dart';

class CarbyneTheme {
  static const double strongOpacity = 0.9;
  static const double weakOpacity = 0.7;
  // the size where we switch font sizes
  static const int deviceWidthCutoff = 700;
  static const double sizeFactorForBiggerFont = 1.3;

  static Color _textColor(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2.color;

    // // this should be some them color - but cannot figure out what color it should be.
    // return Colors.white;
  }

  static Color _textColorDark(BuildContext context) {
    return Theme.of(context).textTheme.bodyText2.color;

    // // this should be some them color - but cannot figure out what color it should be.
    // return Colors.black;
  }

  static double _fontSizeFactor(BuildContext context) {
    double fsf = 1.0;
    final double deviceWidth = MediaQuery.of(context).size.width;
    if (deviceWidth > deviceWidthCutoff) {
      fsf = sizeFactorForBiggerFont;
    }
    return fsf;
  }

  static TextStyle title(BuildContext context) {
    return Theme.of(context).accentTextTheme.headline6.apply(
        color: _textColor(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle titleDark(BuildContext context) {
    return Theme.of(context).accentTextTheme.headline6.apply(
        color: _textColorDark(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle subHeading(BuildContext context) {
    return Theme.of(context).accentTextTheme.subtitle1.apply(
        color: _textColor(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle subHeadingDark(BuildContext context) {
    return Theme.of(context).accentTextTheme.subtitle1.apply(
        color: _textColorDark(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle caption(BuildContext context) {
    return Theme.of(context).accentTextTheme.caption.apply(
        color: _textColor(context).withOpacity(weakOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle captionBold(BuildContext context) {
    return Theme.of(context).accentTextTheme.caption.apply(
        color: _textColor(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle captionDark(BuildContext context) {
    return Theme.of(context).accentTextTheme.caption.apply(
        color: _textColorDark(context).withOpacity(weakOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle captionDarkBold(BuildContext context) {
    return Theme.of(context).accentTextTheme.caption.apply(
        color: _textColorDark(context).withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle captionItalic(BuildContext context) {
    final TextStyle ts = Theme.of(context)
        .accentTextTheme
        .caption
        .copyWith(
            fontStyle: FontStyle.italic,
            color: _textColor(context).withOpacity(weakOpacity))
        .apply(fontSizeFactor: _fontSizeFactor(context));
    return ts;
  }

  static TextStyle captionItalicDark(BuildContext context) {
    final TextStyle ts = Theme.of(context)
        .accentTextTheme
        .caption
        .copyWith(
            fontStyle: FontStyle.italic,
            color: _textColorDark(context).withOpacity(weakOpacity))
        .apply(fontSizeFactor: _fontSizeFactor(context));
    return ts;
  }

  static TextStyle body(BuildContext context) {
    return Theme.of(context).accentTextTheme.bodyText2.apply(
        color: _textColor(context).withOpacity(weakOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle bodyDark(BuildContext context) {
    return Theme.of(context).accentTextTheme.bodyText2.apply(
        color: _textColorDark(context).withOpacity(weakOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle bodyItalic(BuildContext context) {
    final TextStyle ts = Theme.of(context)
        .accentTextTheme
        .bodyText2
        .copyWith(
            fontStyle: FontStyle.italic,
            color: _textColor(context).withOpacity(weakOpacity))
        .apply(fontSizeFactor: _fontSizeFactor(context));
    return ts;
  }

  static TextStyle bodyItalicDark(BuildContext context) {
    final TextStyle ts = Theme.of(context)
        .accentTextTheme
        .bodyText2
        .copyWith(
            fontStyle: FontStyle.italic,
            color: _textColorDark(context).withOpacity(weakOpacity))
        .apply(fontSizeFactor: _fontSizeFactor(context));
    return ts;
  }

  static TextStyle bodyError(BuildContext context) {
    return Theme.of(context).accentTextTheme.bodyText2.apply(
        color: Theme.of(context).errorColor.withOpacity(weakOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static TextStyle bodyBold(BuildContext context) {
    return Theme.of(context)
        .accentTextTheme
        .bodyText2
        .apply(color: _textColor(context).withOpacity(strongOpacity));
  }

  static TextStyle bodyBoldDark(BuildContext context) {
    return Theme.of(context)
        .accentTextTheme
        .bodyText2
        .apply(color: _textColorDark(context).withOpacity(strongOpacity));
  }

  static TextStyle bodyBoldError(BuildContext context) {
    return Theme.of(context).accentTextTheme.bodyText1.apply(
        color: Theme.of(context).errorColor.withOpacity(strongOpacity),
        fontSizeFactor: _fontSizeFactor(context));
  }

  static Color textColorWeak(BuildContext context) {
    return _textColor(context).withOpacity(weakOpacity);
  }

  static Color textColorStrong(BuildContext context) {
    return _textColor(context).withOpacity(strongOpacity);
  }

  static Color textColorWeakDark(BuildContext context) {
    return _textColorDark(context).withOpacity(weakOpacity);
  }

  static Color textColorStrongDark(BuildContext context) {
    return _textColorDark(context).withOpacity(strongOpacity);
  }

  static Color accentColor(BuildContext context) {
    return Theme.of(context).accentColor;
  }

  static Color primaryColor(BuildContext context) {
    return Theme.of(context).primaryColor;
  }

  /// Create a nice looking theme based on the brightness (dark/light)
  static ThemeData buildTheme(Brightness brightness) {
    // Build a dark theme?
    if (brightness == Brightness.dark) {
      return _buildDarkTheme();
    } else {
      return _buildLightTheme();
    }
  }

  // Ok -- make a Dark theme up..
  //   primarySwatch: Colors.green,
  // primaryColor: Color.fromRGBO(51, 59, 78,
  //     1.0), // this is the complimentary background blue from the web app
  // accentColor: Color.fromRGBO(153, 202, 74,
  //     1.0), // this is MM bright green, useful for small accents
  // buttonColor: Color.fromRGBO(74, 144, 226, 1.0),
  // iconTheme: IconThemeData(color: Colors.white),
  // dividerColor: Colors.white,

  static ThemeData _buildDarkTheme() {
    debugPrint('[_buildDarkTheme()');
    return ThemeData.dark().copyWith(
      // this is the complimentary background blue from the web app
      primaryColor: const Color.fromRGBO(51, 59, 78, 1.0),
      // this is MM bright green, useful for small accents
      accentColor: const Color.fromRGBO(153, 202, 74, 1.0),
      dividerColor: Colors.white,
      // textTheme: ThemeData.dark().textTheme.apply(
      //       bodyColor: Colors.white,
      //       displayColor: Colors.white,
      //     ),
      // backgroundColor: Colors.black,
    );
  }

  // Make a nice light and fluffy -- almost weasel like -- theme
  static ThemeData _buildLightTheme() {
    debugPrint('[_buildLightTheme()');
    return ThemeData.light().copyWith(
        // textTheme: ThemeData.light().textTheme.apply(
        //       bodyColor: Colors.black,
        //       displayColor: Colors.black,
        //     ),
        // backgroundColor: Colors.white,
        );
  }

  static BoxDecoration boxDecoration(Brightness brightness) {
    BoxDecoration boxDecoration;
    if (brightness == Brightness.dark) {
      boxDecoration = const BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/images/triangle-bg.png'),
        fit: BoxFit.cover,
        // colorFilter: ColorFilter.mode(Colors.black54, BlendMode.dstATop),
      ));
    } else {
      boxDecoration = const BoxDecoration(
          color: Color.fromRGBO(250, 246, 246, 1.0)
          // colorFilter: ColorFilter.mode(Colors.black54, BlendMode.dstATop),
          );
    }

    return boxDecoration;
  }
}
