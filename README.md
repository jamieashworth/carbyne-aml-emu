# Carbyne iOS AML Emulator

Carbyne AML emulation for demo purposes. This is a little background app that will POST AML type data to the
AML micro server every 15 seconds (default -- changeable). Now, this isn't real AML data in that we can not
access the HELO location (advanced location -- (H)ybridized (E)mergency (Lo)cation). However, it does post enough AML information that should allow c-Lite and c-Live to show our position on the map when doing a demo.

## Getting Started

### Development Environment

This assumes you have a Mac. You will need to have XCode and also the full Flutter/Dart ecosystem installed and running.

In addition this uses fastlane to deploy -- and that is dependent on a few things as well, notably Ruby. Currently the version of Ruby that _MUST_ be used is 2.7.X. If you don't have this version -- use Homebrew (brew) to install it

$ brew install ruby@2.7

Make sure you adjust your PATH variable after installing so that it comes first before the system Ruby (which is older).

## Building the App (release to TestFlight)

To build and release a new version, make sure you update the version # in the pubspec.yaml file to something greater than the previous version, otherwise it will not update.

Also, the git repo needs to be clean so make sure to commit / push any and all changes before submitting to Testflight.

From the command line in the main folder do:
$ cd ios
$ bundle exec fastlane beta

If you get some odd errors you may need to clean out the gem files and re-install.

$ rm Gemfile.lock
$ bundle install

This is a very easy application to run with minimal setup and even less interaction needed to get the needed results!

### Installation

This app will be handled by an Apple system called TestFlight. You will need to go to the Apple App Store and download and install TestFlight onto any/all iOS iPhones you will want to use when demoing our systems.

Note: When you login to TestFlight make sure you use the same AppleId that the phone itself is registered to (i.e. your iCloud login). This is also the email that Operations (or whomever) will need to send you the invite to run the application.

### Setup

The first you launch the app you (should) be shown the Settings screen. This same screen is available via the hamburger icon on the top left of the Application Bar.

The first field is a simple selection that changes the background theme from Dark / Light.

Next is important -- it dicates which of our systems your Location information is sent to. For almost all demo cases the Prod System should be selected. One you touch Prod it will open up a second option to indicate if you want USA or UK. Again, right now the most likely option is USA.

The other System options are for testing purposes by QA and Operations, etc.

Finally there are 3 fields that must be manually entered before this works:

-- This Phone's #
This should be pretty self explanatory. It's the phone # of the phone that you'll use to call into the system for the demo.

-- IMEI Number & ICCID Number.
These two numbers are not as critical in a demo siutation but still should be filled in. On an iPhone the easiest method is to switch to your Settings -> General -> About screens and the numbers are about half-way down the list of information. Copy a number and then go back and Paste it in the appropriate field.

Once you have the form complete simply hit Save and you are done!

### Allow access to location information

The last thing will be to Allow the application to use your Location. As this is the main purpose of the app it's very important to allow location access.

### Basic of what the App does

#### Home Screen

The home screen has the Emu Logo and shows the information that is (hopefully) being posted to the appropriate AML service.

Most of the information is pretty self explanatory how there are a few things to note:

-- Accuracy: Apple desires to have a CONFIDENCE level of 90% for the location. However, since we don't have access to the HELO/ELD location information this means our accuracy indoors will be 65 meters or so. Outdoors we can get it down to 10 meters if we stay fixed in a position (and especially if we can get WiFi singals). Just something to note for Sales.

-- After the block of raw data you should hopefully see a giant SUCCESS! This means that the location you see has been successfully sent to the AML service in the cloud. It also shows you the last time we got a location from the device (Apple doesn't always update location if you don't move) as well as when we last sent it to the cloud.

-- We do not send any updates unless we notice a change of 5m or more in position. This saves battery life.

#### Map

If you hit the little Map Icon in the lower right, it will present a map (fairly well zoomed in) of where it thinks you are and where it reported you. Unless you are very good with Lat/Long locations it's probably a good idea to double check to make sure we're in the right neighborhood since this is what will show in the c-Lite/c-Live map!!

You can pinch in and pinch out to zoom like a normal map. There isn't much else going on here for that.

## Conclusion

That's it! Pretty basic but hopefully it will do the trick and our iOS demos will be all that much happier!
